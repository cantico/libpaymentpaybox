; <?php/*
[general]
name                          = "LibPaymentPaybox"
version                       = "0.2.2"
addon_type                    = "LIBRARY"
mysql_character_set_database  = "latin1,utf8"
encoding                      = "UTF-8"
description                   = "Paybox Payment Gateway functionality"
description.fr                = "Librairie partagée de passerelle de paiement vers Paybox"
delete                        = "1"
longdesc                      = ""
ov_version                    = "8.1.98"
php_version                   = "5.2.0"
addon_access_control          = "0"
configuration_page            = "systemconf"
db_prefix                     = "libpaymentpaybox"
author                        = "Cantico ( support@cantico.fr )"
icon                          = "credit-card.png"
tags						  = "library,payment"

[addons]
widgets						  = "1.0.10"

;*/ ?>