<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';




class libpaymentpaybox_Configuration {

	/**
	 * @var string Name of this configuration.
	 */
	public $name;

	/**
	 * @var string Optional description of this configuration.
	 */
	public $description;
	
	/**
	 * method: cgi | hmac
	 * @var string
	 */
	public $method;

	/**
	 * @var string	Path to cgi executable used for encoding a request to the payment server.
	 */
	public $requestFile;

	public $site;

	public $rank;

	public $identifier;

	public $paybox;

	public $text;

	public $language;

	public $allowedResponseIp;
	
	/**
	 * PBX_SECRET
	 * @var string
	 */
	public $secret;
}




/**
 *
 * @return bab_registry
 */
function libpaymentpaybox_getRegistry()
{
	$registry = bab_getRegistryInstance();

	$registry->changeDirectory('/LibPaymentPaybox');

	return $registry;
}



/**
 * Returns the names of all configurations.
 *
 * @return array
 */
function libpaymentpaybox_getConfigurationNames()
{
	$registry = libpaymentpaybox_getRegistry();

	$registry->changeDirectory('configurations');

	$names = array();

	while ($name = $registry->fetchChildDir()) {
		$name = substr($name, 0, -1);
		$names[$name] = $name;
	}

	return $names;
}





/**
 * Returns all configuration information in a libpaymentpaybox_Configuration object.
 *
 * @param string $name		The configuration name.
 *
 * @return libpaymentpaybox_Configuration
 */
function libpaymentpaybox_getConfiguration($name = null)
{
	$registry = libpaymentpaybox_getRegistry();

	if (!isset($name)) {
		$name = libpaymentpaybox_getDefaultConfigurationName();
	}

	$registry->changeDirectory('configurations');
	$registry->changeDirectory($name);

	$configuration = new libpaymentpaybox_Configuration();

	while ($key = $registry->fetchChildKey()) {
		$configuration->$key = $registry->getValue($key);
	}

	return $configuration;
}





/**
 * Sets the default configuration name.
 *
 * @param string $name		The configuration name.
 *
 * @return void
 */
function libpaymentpaybox_setDefaultConfigurationName($name)
{
	$registry = libpaymentpaybox_getRegistry();
	$identifier = $registry->setKeyValue('default', $name);
}





/**
 * Returns the default configuration name.
 *
 * @return string
 */
function libpaymentpaybox_getDefaultConfigurationName()
{
	$registry = libpaymentpaybox_getRegistry();
	return $registry->getValue('default');
}





/**
 * Renames a configuration.
 *
 * @param string $originalName
 * @param string $newName
 *
 * @return bool
 */
function libpaymentpaybox_renameConfiguration($originalName, $newName)
{
	$registry = libpaymentpaybox_getRegistry();

	$registry->changeDirectory('configurations');

	return $registry->moveDirectory($originalName, $newName);
}


/**
 * Sets all configuration information.
 *
 * @param string name The name of the configuration to save.
 * @param libpaymentpaybox_Configuration $configuration
 *
 * @return void
 */
function libpaymentpaybox_saveConfiguration($name, libpaymentpaybox_Configuration $configuration)
{
	$registry = libpaymentpaybox_getRegistry();

	$registry->changeDirectory('configurations');
	$registry->changeDirectory($name);

	$registry->setKeyValue('name', $configuration->name);
	$registry->setKeyValue('description', $configuration->description);
	$registry->setKeyValue('method', $configuration->method); // cgi | hmac
	$registry->setKeyValue('requestFile', $configuration->requestFile); // cgi only
	$registry->setKeyValue('site', $configuration->site);
	$registry->setKeyValue('rank', $configuration->rank);
	$registry->setKeyValue('identifier', $configuration->identifier);
	$registry->setKeyValue('paybox', $configuration->paybox);
	$registry->setKeyValue('text', $configuration->text);
	$registry->setKeyValue('language', $configuration->language);
	$registry->setKeyValue('allowedResponseIp', $configuration->allowedResponseIp);
	$registry->setKeyValue('secret', $configuration->secret); // hmac only
}





/**
 * Deletes the specified configuration.

 * @param string name The name of the configuration to delete.
 * @return bool
 */
function libpaymentpaybox_deleteConfiguration($name)
{
	$registry = libpaymentpaybox_getRegistry();
	$registry->changeDirectory('configurations');

	if (!$registry->isDirectory($name)) {
		return false;
	}

	$registry->changeDirectory($name);

	$registry->deleteDirectory();

	return true;
}
