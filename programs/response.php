<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */

/**
 * This script is called by the paybox payment after each payment.
 *
 * The names of the parameters is defined in PBX_RETOUR parameter in Func_Payment_Paybox::getRequestCommandLine().
 */




/**
 * Unique ID corresponding to the payment.
 * @var string $ref
 */
$ref = bab_rp('ref');

/**
 * Transaction number.
 * @var string $trans
 */
$trans = bab_rp('trans');

/**
 * Authorization number.
 * @var string $authorization
 */
$authorization = bab_rp('authorization');


/**
 * The payment amount.
 * @var string $amount
 */
$amount = bab_rp('amount');

/**
 * Error code.
 * @var string $error
 */
$error = bab_rp('error');

/**
 * @var string $signature
 */
$signature = bab_rp('signature');



bab_debug($_REQUEST, 'payment');




/* @var $Paybox Func_Payment_Paybox */
$Paybox = bab_Functionality::get('Payment/Paybox');


try {

	$Paybox->checkResponse($amount, $ref, $authorization, $trans, $error, $signature);

} catch (libpayment_AuthorisationException $e) {

	// erreur lors de l'autorisation sur la passerelle de paiement, notifier l'application
	
	$paymentEvent = $Paybox->newEventPaymentError();
	$paymentEvent->errorCode = $e->getCode();
	$paymentEvent->errorMessage = $e->getMessage();
	$paymentEvent->setPayment($e->getPayment());
	
	// copy public properties to event
	
	foreach(get_object_vars($e) as $name => $value)
	{
		$paymentEvent->$name = $value;
	}
	
	bab_fireEvent($paymentEvent);
	
	return;
} catch (libpayment_Exception $e) {

	// erreur interne, la passerelle de paiement n'a pas pu etre appellee ou des parametres sont manquants

	bab_debug($e->getMessage()."\n".$e->getCommandLine()."\n".$e->getGatewayMessage());
	return;
}


// There was no exception thrown by checkResponse(), the payment was successful.

$paymentLogSet = new payment_logSet();

$paymentLog = $paymentLogSet->get($paymentLogSet->token->is($ref));

if (!$paymentLog) {
	bab_debug(sprintf('payment token not found in log (%s)', $ref));
	return;
}

$responseData = array(
	'amount' => $amount,
	'ref' => $ref,
	'authorization' => $authorization,
	'trans' => $trans,
	'error' => $error,
	'signature' => $signature
);

$paymentLog->response = serialize($responseData);
$paymentLog->save();

bab_debug($paymentLog);
$payment = unserialize($paymentLog->payment);

bab_debug($payment);

$paymentEvent = $Paybox->newEventPaymentSuccess($authorization, $trans);

$paymentEvent->setPayment($payment);
$paymentEvent->setResponseAmount($amount / 100);
$paymentEvent->setResponseAuthorization($authorization);
$paymentEvent->setResponseTransaction($trans);

bab_debug($paymentEvent);

bab_fireEvent($paymentEvent);


