<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */

/**
 * This script is called by the paybox payment server when an error occurred.
 *
 * A parameter NUMERR contains the paybox error code.
 */


$err = bab_rp('NUMERR', '');

switch($err)
{
	case '-1':
		$err_txt = 'Error in reading the parameters via stdin (POST method) (error in http reception).'; break;
	case '-2':
	    $err_txt = "Error  in memory allocation. Not enough memory available on the trader's server."; break;
	case '-3':
	    $err_txt = "Error in reading the parameters QUERY_STRING or CONTENT_LENGTH. (http error)."; break;
	case '-4':
		$err_txt = "PBX_RETOUR, PBX_ANNULE, PBX_REFUSE or PBX_EFFECTUE are too long (<150 characters)."; break;
	case '-5':
		$err_txt = "Error in opening the file (if PBX_MODE contains 3) : local file non-existent, not found or access error."; break;
	case '-6':
		$err_txt = "Error in file format (if PBX_MODE contains 3) : local file badly formed, empty or lines are badly formatted."; break;
	case '-7':
		$err_txt = "A compulsory variable is missing (PBX_SITE, PBX_RANG, PBX_IDENTIFIANT, PBX_TOTAL, PBX_CMD,etc.)"; break;
	case '-8':
		$err_txt = "One of the numerical variables contains a non-numerical character (site, rank, identifier, amount, currency etc. )"; break;
	case '-9':
		$err_txt = "PBX_SITE contains a site number which does not consist of exactly 7 characters."; break;
	case '-10':
		$err_txt = "PBX_RANG contains a rank number which does not consist of exactly 2 characters."; break;
	case '-11':
		$err_txt = "PBX_TOTAL has more than 10 or fewer than 3 numerical characters."; break;
	case '-12':
		$err_txt = "PBX_LANGUE or PBX_DEVISE contains a code which does not contain exactly 3 characters."; break;
	case '-13':
		$err_txt = "PBX_CMD is empty or contains a reference longer than 250 characters."; break;
	case '-14':
		$err_txt = "Not used"; break;
	case '-15':
		$err_txt = "Not used"; break;
	case '-16':
		$err_txt = "PBX_PORTEUR does not contain a valid e-mail address."; break;
	case '-17':
		$err_txt = "Error of coherence (multi-baskets) : Reserved Future Usage"; break;
	default:
		$err_txt = '';
		break;
}
$babBody->addError($err.' '.$err_txt);

$PaymentSystem = bab_functionality::get('Payment/Paybox');

$errorEvent = $PaymentSystem->newEventPaymentError();
$errorEvent->errorCode = $err;
$errorEvent->errorMessage = $err_txt;

bab_fireEvent($errorEvent);




