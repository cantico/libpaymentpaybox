<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/configuration.php';
require_once dirname(__FILE__) . '/paymentpage.class.php';
bab_functionality::includeFile('Payment');


class libpayment_Payment_Paybox extends libpayment_Payment
{
}




class Func_Payment_Paybox extends Func_Payment
{

	private $PBX_EFFECTUE = '';
	private $PBX_ANNULE = '';
	private $PBX_REFUSE = '';
	
	private $currencies = array(
		'EUR' => '978',
		'USD' => '840',
		'CHF' => '756',
		'GBP' => '826',
		'CAD' => '124',
		'JPY' => '392',
		'MXP' => '484',
		'TRL' => '792',
		'AUD' => '036',
		'NZD' => '554',
		'NOK' => '578',
		'BRC' => '986',
		'ARP' => '032',
		'KHR' => '116',
		'TWD' => '901',
		'SEK' => '752',
		'DKK' => '208',
		'KRW' => '410',
		'SGD' => '702'
	);
	
	/**
     * @param string $url
     * @return libpayment_Payment
     */
    public function setDoneUrl($url)
    {
    	$this->PBX_EFFECTUE = $url;
    	return $this;
    }
	
	/**
     * @param string $url
     * @return libpayment_Payment
     */
    public function setCancelUrl($url)
    {
    	$this->PBX_ANNULE = $url;
    	return $this;
    }
	
	/**
     * @param string $url
     * @return libpayment_Payment
     */
    public function setRefusedUrl($url)
    {
    	$this->PBX_REFUSE = $url;
    	return $this;
    }


	/**
	 * (non-PHPdoc)
	 * @see Func_Payment::getDescription()
	 */
	public function getDescription()
	{
		return libpaymentpaybox_translate('Paybox Payment Gateway');
	}


	private function getCurrentLang()
	{
		switch($GLOBALS['babLanguage'])
		{
			case 'fr':
				return 'FRA';

			case 'en':
				return 'GBR';

			case 'es':
				return 'ESP';

			case 'de':
				return 'GDR';

			case 'it':
				return 'ITA';

			case 'nl':
			case 'nl-be':
				return 'NLD';

			default:
				return 'GBR';
		}

		return '';
	}


	/**
	 * @param string $currency A libpayment_Payment::CURRENCY_xxx code.
	 */
	private function getCurrencyCode($currency)
	{
		if (isset($this->currencies[$currency])) {
			return $this->currencies[$currency];
		}

		return '';
	}
	
	
	/**
	 * Unsigned parameters common used by cgi and hmac
	 * @return array
	 */
	protected function getParameters(libpayment_Payment $payment)
	{
	    global $babUrl;
	    $configuration = libpaymentpaybox_getConfiguration();
	    
	    return array(
	        'PBX_MODE' => '4',
	        'PBX_SITE' => $configuration->site,
	        'PBX_RANG' => $configuration->rank,
	        'PBX_IDENTIFIANT' => $configuration->identifier,
	        'PBX_PAYBOX' => $configuration->paybox,
	    
	        // Text (in html format) displayed on the intermediate page.
	        'PBX_TXT' => $configuration->text,
	    
	        'PBX_LANG' => $configuration->language,
	    
	        // Amount in cents without dot or comma.
	        'PBX_TOTAL' => round($payment->getAmount(), 2) * 100,
	    
	        // ISO 4217 numerical code of currency
	        'PBX_DEVISE' => $this->getCurrencyCode($payment->getCurrency()),
	    
	        // Email address of purchaser (@ and . must be present).
	        'PBX_PORTEUR' => $payment->getEmail(),
	    
	        'PBX_RETOUR' => 'amount:M;ref:R;authorization:A;trans:T;error:E;signature:K',
	    
	        'PBX_EFFECTUE' => $this->PBX_EFFECTUE, //$babUrl . bab_Url::request_gp(), // $babUrl.$Crm->Controller()->ShoppingCart()->creditCardReturn()->url()
	        'PBX_ANNULE' => $this->PBX_ANNULE, //$babUrl, // . bab_Url::request_gp(),// $babUrl.$Crm->Controller()->ShoppingCart()->edit($cartRecord->id)->url();
	        'PBX_REFUSE' => $this->PBX_REFUSE, //$babUrl, // . bab_Url::request_gp(),// $babUrl.$Crm->Controller()->ShoppingCart()->edit($cartRecord->id)->url();
	    
	        // Secure URL called by Paybox
	        'PBX_REPONDRE_A' => $babUrl. '?tg=addon/LibPaymentPaybox/response',// $babUrl.$Crm->Controller()->ShoppingCart()->creditCardResponse()->url();
	    
	        'PBX_ERREUR' => $babUrl. '?tg=addon/LibPaymentPaybox/pberror', // escapeshellarg($addon->getUrl().'pberror'),
	    
	        'PBX_CMD' => $payment->getToken(),
	    );
	}


	/**
	 * Returns the command line used to make a payment server request.
	 *
	 * @param  libpayment_Payment $payment
	 * @return string
	 */
	protected function getRequestCommandLine(libpayment_Payment $payment)
	{
		
		$configuration = libpaymentpaybox_getConfiguration();

		$commandLine = '"'.$configuration->requestFile.'"';

		$parameters = $this->getParameters($payment);
		
		// No specific parameters to the CGI method?
		
		foreach ($parameters as $parameterName => $parameterValue) {
			$commandLine .= ' ' . escapeshellarg($parameterName . '=' . $parameterValue);
		}


		return $commandLine;
	}



	/**
	 * Get the best available algo (HMAC method)
	 * @return string
	 */
	protected function getBestAlgo()
	{
	    $hashs = array(
	        'sha512',
	        'sha256',
	        'sha384',
	        'ripemd160',
	        'sha224',
	        'mdc2'
	    );
	    
	    $hashEnabled = hash_algos();

	    foreach($hashs as $hash){
	        if(in_array($hash, $hashEnabled)){
	            return $hash;
	        }
	    }
	    
	    return null;
	}

	
	/**
	 * add PBX_HASH and PBX_HMAC to parameters
	 * @param array &$parameters
	 */
	public function addHmac($parameters) {
	    
	    $configuration = libpaymentpaybox_getConfiguration();
	    
	    $parameters['PBX_HASH'] = $this->getBestAlgo();
	    
	    if(isset($parameters['PBX_HMAC'])){
	        unset($parameters['PBX_HMAC']);
	    }
	
	    $param = '';
	    foreach($parameters as $key => $value){
	        $param .= "&".$key.'='.$value;
	    }
	
	    $param = ltrim($param,'&');
	
	    $binkey = pack('H*', $configuration->secret);
	
	    $parameters['PBX_HMAC'] = strtoupper(hash_hmac($parameters['PBX_HASH'], $param, $binkey));
	    
	    return $parameters;
	}
	
	
	
	
	
	/**
	 * CGI method
	 *
	 * @param libpayment_Payment $payment The payment object containing all relevant information about the payment to perform.
	 *
	 * @return string	The HTML code to display that will allow the user to perform the payment.
	 *
	 * @throws libpayment_Exception
	 */
	private function getCgiPaymentRequestHtml(libpayment_Payment $payment)
	{
	    $commandLine = $this->getRequestCommandLine($payment);
	
	    // Execution of paybox cgi should be done using shell_exec according to paybox documentation.
	    $result = shell_exec($commandLine);
	
	    if (!isset($result)) {
	
	        // shell_exec can return NULL both when an error occurs or the program produces no output
	        // retry with exec to get the exit code
	
	        exec($commandLine, $output, $return_var);
	
	        bab_debug($commandLine);
	        throw new libpayment_Exception('Failed to execute paybox command line, return status '.$return_var);
	    }
	
	    
	
	    return $result;
	}
	
	
	
	/**
	 * HMAC method
	 *
	 * @param libpayment_Payment $payment The payment object containing all relevant information about the payment to perform.
	 *
	 * @return string	The HTML code to display that will allow the user to perform the payment.
	 */
	private function getHmacPaymentRequestHtml(libpayment_Payment $payment)
	{
	    $parameters = $this->getParameters($payment);
	    
	    $parameters = $this->addHmac($parameters);
	    
	    $page = new LibPaymentPaybox_PaymentPage($parameters);
	    
	    return $page->getHtml();
	}
	
	



	/**
	 * Returns the html code to display to allow the user to perform the payment with the payment server.
	 *
	 * @param libpayment_Payment $payment The payment object containing all relevant information about the payment to perform.
	 *
	 * @return string	The HTML code to display that will allow the user to perform the payment.
	 *
	 * @throws libpayment_Exception
	 */
	public function getPaymentRequestHtml(libpayment_Payment $payment)
	{
	    $configuration = libpaymentpaybox_getConfiguration();
	    
	    switch($configuration->method) {
	        
	        case 'hmac':
	            $html = $this->getHmacPaymentRequestHtml($payment);
	            break;
	        
	        default:
	        case 'cgi':
	            $html = $this->getCgiPaymentRequestHtml($payment);
	            break;
	    }
	    
	    $this->logPayment($payment);
	    
	    return $html;
	}




	/**
	 * @throws libpayment_Exception
	 * @return bool
	 */
	public function checkResponse($amount, $ref, $authorization, $trans, $error, $signature)
	{
		
		$configuration = libpaymentpaybox_getConfiguration();

		bab_debug($_SERVER['REMOTE_ADDR']);

		if (trim($configuration->allowedResponseIp) != '') {
			$allowedResponseIp = explode(',', $configuration->allowedResponseIp);
			if (!in_array($_SERVER['REMOTE_ADDR'], $allowedResponseIp)) {
				throw new libpayment_Exception(libpaymentpaybox_translate('Discard not allowed IP address from paybox response '.$_SERVER['REMOTE_ADDR']));
			}
		}

		if (!$this->checkSignature($amount, $ref, $authorization, $trans, $error, $signature)) {
			throw new libpayment_Exception(sprintf(libpaymentpaybox_translate('Failed signature verification for reference %s'), $ref));
		}

		if ('00000' !== $error)
		{
			switch ($error) {
				case '00000':
					$errorMessage = libpaymentpaybox_translate('Transaction successful.');
					break;
				case '00010':
					$errorMessage = libpaymentpaybox_translate('Unknown currency.');
					break;
				case '00003':
					$errorMessage = libpaymentpaybox_translate('Paybox error.');
					break;
				case '00006':
					$errorMessage = libpaymentpaybox_translate('Paybox error.');
					break;
				case '00008':
					$errorMessage = libpaymentpaybox_translate('Paybox error.');
					break;
				case '00009':
					$errorMessage = libpaymentpaybox_translate('Paybox error.');
					break;
				default:
					$errorMessage = '';
					break;
			}
			
			$exception = new libpayment_AuthorisationException($errorMessage);
			
			
			if (!$exception->initPaymentFromToken($ref))
			{
				throw new libpayment_Exception(sprintf('Received payment response for non existing payment token (%s)', $result['caddie']));
			}
			
			$exception->response_code = $error;
			throw $exception;
		}


		if (empty($trans)) {
			throw new libpayment_Exception(libpaymentpaybox_translate('The paybox transaction number is missing'));
		}


		if (empty($authorization)) {
			throw new libpayment_Exception(libpaymentpaybox_translate('The paybox authorization number is missing'));
		}


		return true;
	}




	/**
	 * Verify data signature using openSSL and paybox public key.
	 *
	 * @param string $amount
	 * @param string $ref
	 * @param string $authorization
	 * @param string $trans
	 * @param string $error
	 * @param string $signature
	 */
	protected function checkSignature($amount, $ref, $authorization, $trans, $error, $signature)
	{
		$data = 'amount='.urlencode($amount);
		$data .= '&ref='.urlencode($ref);
		$data .= '&authorization='.urlencode($authorization);
		$data .= '&trans='.urlencode($trans);
		$data .= '&error='.urlencode($error);

		$signature = base64_decode($signature);

		$filedata = file_get_contents(dirname(__FILE__). '/keys/pubkey.pem');

		$key = openssl_pkey_get_public($filedata);

		if (!openssl_verify($data, $signature, $key)) {
			return false;
		}

		return true;
	}



	public function getTpeList()
	{
		$tpeNames = libpaymentpaybox_getConfigurationNames();

		return $tpeNames;
	}


}
