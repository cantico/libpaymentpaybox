<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/


/**
 * Translates a text string.
 *
 * @param string $str The string to translate.
 *
 * @return string The translated string.
 */
function libpaymentpaybox_translate($str)
{
    return bab_translate($str, 'LibPaymentPaybox');
}





/**
 * @return Func_Widgets
 */
function libpaymentpaybox_Widgets()
{
	$W = bab_Functionality::get('Widgets');
	return $W;
}





/**
 * Returns the list of possible languages for the language parameter.
 *
 * The keys of the returned array are codes usable for the PBX_LANG parameter.
 *
 * @return multitype:string
 */
function libpaymentpaybox_getAvailableLanguages()
{
	static $languages = null;
	if (!isset($languages)) {

		$languages = array(
			'FRA' => libpaymentpaybox_translate('French'),
			'GBR' => libpaymentpaybox_translate('English'),
			'ESP' => libpaymentpaybox_translate('Spanish'),
			'ITA' => libpaymentpaybox_translate('Italian'),
			'DEU' => libpaymentpaybox_translate('German'),
			'NLD' => libpaymentpaybox_translate('Dutch'),
			'SWE' => libpaymentpaybox_translate('Swedish'),
		);
	}

	return $languages;
}
