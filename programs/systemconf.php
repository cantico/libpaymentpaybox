<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/configuration.php';


/* @var $I Func_Icons */
$I = bab_functionality::get('Icons');
$I->includeCss();



/**
 *
 * @return Widget_Frame
 */
function libpaymentpaybox_TpeEditor()
{
	$W = libpaymentpaybox_Widgets();
	$editor = $W->Frame();

	$languageSelect = $W->Select();

	$languageSelect->addOption('', '');
	$availableLanguages = libpaymentpaybox_getAvailableLanguages();
	foreach ($availableLanguages as $languageCode => $languageName) {
		$languageSelect->addOption($languageCode, $languageName);
	}
	
	
	
	$requestFile = $W->LabelledWidget(
	    libpaymentpaybox_translate('Path to executable CGI program'),
	    $W->LineEdit()
    	    ->addClass('widget-fullwidth')
    	    ->setSize(80),
	    'requestFile'
	);
	
	$secret = $W->LabelledWidget(
        libpaymentpaybox_translate('Exchange private key (PBX_SECRET)'),
        $W->LineEdit()
            ->addClass('widget-fullwidth')
            ->setSize(80),
        'secret',
	    libpaymentpaybox_translate('Available in the paybox back-office')
    );
	
	

	$layout = $W->VBoxItems(

		$W->LabelledWidget(
			libpaymentpaybox_translate('Name'),
			$W->LineEdit()
				->setMandatory(true, libpaymentpaybox_translate('You must specify a unique name for this TPE.'))
				->addClass('widget-fullwidth'),
			'name'
		),
		$W->LabelledWidget(
			libpaymentpaybox_translate('Description'),
			$W->TextEdit()
				->setLines(1)
				->addClass('widget-fullwidth'),
			'description'
		),
		$W->LabelledWidget(
			libpaymentpaybox_translate('Site number (TPE) provided by the bank'),
			$W->LineEdit()
				->addClass('widget-fullwidth'),
			'site'
		),
		$W->LabelledWidget(
			libpaymentpaybox_translate('Rank number (machine) provided by the bank'),
			$W->LineEdit()
				->addClass('widget-fullwidth'),
			'rank'
		),
		$W->LabelledWidget(
			libpaymentpaybox_translate('Paybox identifier provided by PAYBOX SERVICES'),
			$W->LineEdit()
				->addClass('widget-fullwidth'),
			'identifier'
		),
		$W->LabelledWidget(
			libpaymentpaybox_translate('Text to be displayed on the intermediate page'),
			$W->SimpleHtmlEdit()
				->addClass('widget-fullwidth'),
			'text'
		),
		$W->LabelledWidget(
			libpaymentpaybox_translate('Language used by Paybox to display the payment page'),
			$languageSelect,
			'language'
		),

	    $W->LabelledWidget(
	        libpaymentpaybox_translate('Method'),
	        $W->Select()
	        ->addOption('cgi', 'CGI')
	        ->addOption('hmac', 'HMAC')
	        ->setAssociatedDisplayable($requestFile, array('cgi'))
	        ->setAssociatedDisplayable($secret, array('hmac')),
	        'method'
	        
	    ),

		$requestFile,
	    
	    $secret,
	    

		$W->LabelledWidget(
			libpaymentpaybox_translate('Paybox CGI url'),
			$W->LineEdit()
				->addClass('widget-fullwidth')
				->setSize(80),
			'paybox',
		    'https://tpeweb.paybox.com/cgi/MYchoix_pagepaiement.cgi'
		),

		$W->LabelledWidget(
			libpaymentpaybox_translate('Allowed response IP (comma separated list)'),
			$W->LineEdit()
				->addClass('widget-fullwidth')
				->setSize(80),
		    'allowedResponseIp',
		    libpaymentpaybox_translate('Not recommended by paybox')
		)

	)->setVerticalSpacing(1, 'em');

	$editor->setLayout($layout);

	return $editor;
}



/**
 *
 * @param string $tpe
 */
function libpaymentpaybox_editTpe($tpe = null)
{
	$W = libpaymentpaybox_Widgets();

	$addon = bab_getAddonInfosInstance('LibPaymentPaybox');
	$addonUrl = $addon->getUrl();


	$page = $W->babPage();
	$page->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));



	$form = $W->Form();
	$form->addClass('BabLoginMenuBackground');
	$form->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));
	$form->setName('tpe');

	$editor = libpaymentpaybox_TpeEditor();

	$form->addItem($editor);

	$form->addItem(
		$W->FlowItems(
			$W->SubmitButton()
				->setLabel(libpaymentpaybox_translate('Save configuration'))
				->addClass('icon ' . Func_Icons::ACTIONS_DIALOG_OK),
			$W->Link(
				libpaymentpaybox_translate('Cancel'),
				$addonUrl . 'systemconf&idx=displayTpeList'
			)->addClass('icon ' . Func_Icons::ACTIONS_DIALOG_CANCEL)
		)
		->setSpacing(1, 'em')
		->addClass(Func_Icons::ICON_LEFT_16)
	);

	$form->setHiddenValue('tg', bab_rp('tg'));
	$form->setHiddenValue('idx', 'saveTpe');

	if (isset($tpe)) {
		$title = libpaymentpaybox_translate('Edit TPE configuration');

		$configuration = libpaymentpaybox_getConfiguration($tpe);
		$form->setValues(
			array(
				'tpe' => array(
					'name' => $configuration->name,
					'description' => $configuration->description,
					'identifier' => $configuration->identifier,
					'language' => $configuration->language,
					'paybox' => $configuration->paybox,
					'rank' => $configuration->rank,
					'site' => $configuration->site,
					'text' => $configuration->text,
				    'method' => $configuration->method,
					'requestFile' => $configuration->requestFile,
					'allowedResponseIp' => $configuration->allowedResponseIp,
				    'secret' => $configuration->secret
				)
			)
		);
		$form->setHiddenValue('tpe[originalName]', $tpe);
	} else {
		$title = libpaymentpaybox_translate('New TPE configuration');
	}

	$page->setTitle($title);

	$page->addItem($form);

	$page->displayHtml();
}




function libpaymentpaybox_saveTpe($tpe)
{
	$configuration = new libpaymentpaybox_Configuration();

	$configuration->name = $tpe['name'];
	$configuration->description = $tpe['description'];
	$configuration->identifier = $tpe['identifier'];
	$configuration->language = $tpe['language'];
	$configuration->paybox = $tpe['paybox'];
	$configuration->rank = $tpe['rank'];
	$configuration->site = $tpe['site'];
	$configuration->text = $tpe['text'];
	$configuration->method = $tpe['method'];
	$configuration->requestFile = $tpe['requestFile'];
	$configuration->allowedResponseIp = $tpe['allowedResponseIp'];
	$configuration->secret = $tpe['secret'];

	if (isset($tpe['originalName']) && $tpe['name'] != $tpe['originalName']) {
		libpaymentpaybox_renameConfiguration($tpe['originalName'], $tpe['name']);
	}

	libpaymentpaybox_saveConfiguration($tpe['name'], $configuration);
}




function libpaymentpaybox_displayTpeList()
{
	$W = libpaymentpaybox_Widgets();

	$page = $W->babPage();
	$page->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));

	$page->addItem($W->Title(libpaymentpaybox_translate('List of configured TPE')));

	$tpeNames = libpaymentpaybox_getConfigurationNames();


	$tableView = $W->TableView();


	$addon = bab_getAddonInfosInstance('LibPaymentPaybox');
	$addonUrl = $addon->getUrl();

	$tableView->addItem(
		$W->Label(libpaymentpaybox_translate('Name')),
		0, 0
	);
	$tableView->addItem(
		$W->Label(libpaymentpaybox_translate('Site number')),
		0, 1
	);
	$tableView->addItem(
		$W->Label(libpaymentpaybox_translate('Rank number')),
		0, 2
	);
	$tableView->addItem(
		$W->Label(libpaymentpaybox_translate('Paybox identifier')),
		0, 3
	);
	$tableView->addItem(
		$W->Label(''),
		0, 4
	);

	$tableView->addColumnClass(4, 'widget-column-thin');

	$tableView->addSection('tpe');
	$tableView->setCurrentSection('tpe');


	$defaultName = libpaymentpaybox_getDefaultConfigurationName();

	$row = 0;
	foreach ($tpeNames as $name) {

		$configuration = libpaymentpaybox_getConfiguration($name);

		$nameLabel = $W->Label($configuration->name);
		if ($name == $defaultName) {
			$nameLabel->addClass('icon ' . Func_Icons::ACTIONS_DIALOG_OK);
		}

		$tableView->addItem(
			$W->VBoxItems(
				$nameLabel,
				$W->Label($configuration->description)->addClass('widget-small')
			)->addClass(Func_Icons::ICON_LEFT_16),
			$row, 0
		);
		$tableView->addItem(
			$W->Label($configuration->site),
			$row, 1
		);
		$tableView->addItem(
			$W->Label($configuration->rank),
			$row, 2
		);
		$tableView->addItem(
			$W->Label($configuration->identifier),
			$row, 3
		);
		$tableView->addItem(
			$W->FlowItems(
				$W->Link(
					libpaymentpaybox_translate('Edit'),
					$addonUrl . 'systemconf&idx=editTpe&tpe=' . $name
				)->addClass('icon widget-nowrap ' . Func_Icons::ACTIONS_DOCUMENT_EDIT),

				$W->Link(
					libpaymentpaybox_translate('Set default'),
					$addonUrl . 'systemconf&idx=setDefaultTpe&tpe=' . $name
				)->addClass('icon widget-nowrap ' . Func_Icons::ACTIONS_DIALOG_OK),

				$W->Link(
						libpaymentpaybox_translate('Delete'),
						$addonUrl . 'systemconf&idx=deleteTpe&tpe=' . $name
				)->setConfirmationMessage(sprintf(libpaymentpaybox_translate('Are you sure you want to delete the TPE \'%s\'?'), $name))
				->addClass('icon widget-nowrap ' . Func_Icons::ACTIONS_EDIT_DELETE)
			)
			->setSpacing(4, 'px')
			->addClass(Func_Icons::ICON_LEFT_16),
			$row, 4
		);

		$row++;
	}

	$page->addItem($tableView);

	$page->addItem(
		$W->FlowItems(
			$W->Link(
				libpaymentpaybox_translate('Add configuration'),
				$addonUrl . 'systemconf&idx=editTpe'
			)->addClass('icon ' . Func_Icons::ACTIONS_LIST_ADD)
		)->addClass(Func_Icons::ICON_LEFT_16)
	);

	$page->displayHtml();
}




function libpaymentpaybox_deleteTpe($tpe)
{
	libpaymentpaybox_deleteConfiguration($tpe);
}



function libpaymentpaybox_setDefaultTpe($tpe)
{
	libpaymentpaybox_setDefaultConfigurationName($tpe);
}


/* main */

if (!bab_isUserAdministrator())
{
	return;
}



$idx= bab_rp('idx', 'displayTpeList');

$addon = bab_getAddonInfosInstance('LibPaymentPaybox');


switch ($idx)
{
	case 'displayTpeList':
		$babBody->addItemMenu('list', libpaymentpaybox_translate('List'), $GLOBALS['babAddonUrl'] . 'systemconf&idx=displayTpeList');
		libpaymentpaybox_displayTpeList();
		break;

	case 'editTpe':
		$tpe = bab_rp('tpe', null);
		$editor = libpaymentpaybox_editTpe($tpe);
		break;

	case 'saveTpe':
		$tpe = bab_rp('tpe', null);
		$editor = libpaymentpaybox_saveTpe($tpe);
		header('location:'. $addon->getUrl() .'systemconf&idx=displayTpeList');
		break;

	case 'deleteTpe':
		$tpe = bab_rp('tpe', null);
		libpaymentpaybox_deleteTpe($tpe);
		header('location:'. $addon->getUrl() .'systemconf&idx=displayTpeList');
		break;

	case 'setDefaultTpe':
		$tpe = bab_rp('tpe', null);
		libpaymentpaybox_setDefaultTpe($tpe);
		header('location:'. $addon->getUrl() .'systemconf&idx=displayTpeList');
		break;
}
